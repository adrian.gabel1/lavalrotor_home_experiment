import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os
import pprint

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_harddisk.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

UUID_sensor = sensor_settings_dict["ID"]
acceleration_x = []
acceleration_y = []
acceleration_z = []
timestamp = []

sensor_values_dict = {UUID_sensor:{"acceleration_x":acceleration_x, "acceleration_y":acceleration_y,
                                   "acceleration_z":acceleration_z, "timestamp":timestamp}}

# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

sensor_data = []
t_start = time.time()
t_end = time.time() + measure_duration_in_s

while time.time() < t_end:
    sensor_data.append(accelerometer.acceleration)
    timestamp.append(time.time()-t_start)
    time.sleep(0.001)

i = 0
while i < len(sensor_data):
  acceleration_x.append(sensor_data[i][0])
  acceleration_y.append(sensor_data[i][1])
  acceleration_z.append(sensor_data[i][2])
  i = i + 1

sensor_values_dict = {UUID_sensor:{"acceleration_x":acceleration_x, "acceleration_y":acceleration_y,
                                   "acceleration_z":acceleration_z, "timestamp":timestamp}}

# ---------------------------------------------------------------------------------------------#3-end

# -------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

f = h5py.File(path_h5_file, "w")
grp_raw = f.create_group("RawData")

subgrp_raw = grp_raw.create_group(UUID_sensor)

subgrp_raw.create_dataset("acceleration_x", data=acceleration_x)
subgrp_raw.create_dataset("acceleration_y", data=acceleration_y)
subgrp_raw.create_dataset("acceleration_z", data=acceleration_z)
subgrp_raw.create_dataset("timestamp", data=timestamp)

subgrp_raw["acceleration_x"].attrs["unit"] = "m/s^2"
subgrp_raw["acceleration_y"].attrs["unit"] = "m/s^2"
subgrp_raw["acceleration_z"].attrs["unit"] = "m/s^2"
subgrp_raw["timestamp"].attrs["unit"] = "s"

f.close()

# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
